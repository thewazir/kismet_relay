defmodule KismetTcpClient do
	def connect() do
		{:ok, socket} = :gen_tcp.connect({127, 0, 0, 1}, 2501, [:binary, {:packet, 0}], 1000)
		:ok = :gen_tcp.send(socket, '!2 remove TIME\r\n\r\n')
		:ok = :gen_tcp.send(socket, '!2 remove PROTOCOLS\r\n\r\n')
		:ok = :gen_tcp.send(socket, '!2 remove ACK\r\n\r\n')
		:ok = :gen_tcp.send(socket, '!2 remove KISMET\r\n\r\n')
		:ok = :gen_tcp.send(socket, '!2 enable CLIENT mac\r\n\r\n')
		receive_data
	end

	def receive_data do
		receive do
			{:tcp, _, binary} ->
				pid = spawn KismetHttpClient, :post, []
				send pid, {self, binary}
				receive_data
			{:tcp_closed, _} ->
				IO.puts("All done")
		end
	end
end
