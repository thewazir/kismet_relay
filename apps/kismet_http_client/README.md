# KismetHttpClient

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `kismet_http_client` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:kismet_http_client, "~> 0.1.0"}]
    end
    ```

  2. Ensure `kismet_http_client` is started before your application:

    ```elixir
    def application do
      [applications: [:kismet_http_client]]
    end
    ```

